//
//  AppDelegate.h
//  Labb_iOS_2.0
//
//  Created by Kluster on 2016-02-06.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


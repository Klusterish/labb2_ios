//
//  QuizBackend.h
//  Labb_iOS_2.0
//
//  Created by Kluster on 2016-02-06.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizBackend : NSObject

@property (nonatomic) NSArray *questions;
@property (nonatomic) NSDictionary *question0;
@property (nonatomic) NSDictionary *question1;
@property (nonatomic) NSDictionary *question2;
@property (nonatomic) NSDictionary *question3;
@property (nonatomic) NSDictionary *question4;
@property (nonatomic) NSDictionary *question5;
@property (nonatomic) NSDictionary *question6;
@property (nonatomic) NSDictionary *question7;
@property (nonatomic) NSDictionary *question8;
@property (nonatomic) NSDictionary *question9;
@property (nonatomic) NSDictionary *question10;
@property (nonatomic) NSMutableArray *usedQuestions;

-(NSDictionary*)randomQuestion;
-(BOOL)hasThisQuestionBeenUsed:(NSDictionary*)questionToCheck;
-(BOOL)answer1:(NSDictionary*)question;
-(BOOL)answer2:(NSDictionary*)question;
-(BOOL)answer3:(NSDictionary*)question;
-(BOOL)answer4:(NSDictionary*)question;



@end

//
//  QuizBackend.m
//  Labb_iOS_2.0
//
//  Created by Kluster on 2016-02-06.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import "QuizBackend.h"

@implementation QuizBackend

-(instancetype)init {
    self = [super init];
    if(self) {
        self.question0 = @{@"question": @"Who is the founder of Apple?",
                           @"alternative1": @"Alan Turing",
                           @"alternative2": @"Bill Gates",
                           @"alternative3": @"Steve Jobs",
                           @"alternative4": @"Michael Moore",
                           @"rightAnswer": @"Steve Jobs",
                           @"used": @"NO"};
        self.question1 = @{@"question": @"Where are Blizzard Headquarters located?",
                           @"alternative1": @"Irving",
                           @"alternative2": @"Los Angeles",
                           @"alternative3": @"Washington",
                           @"alternative4": @"New York",
                           @"rightAnswer": @"Irving",
                           @"used": @"NO"};
        self.question2 = @{@"question": @"Which element is named FE?",
                           @"alternative1": @"Gold",
                           @"alternative2": @"Silver",
                           @"alternative3": @"Fosforus",
                           @"alternative4": @"Iron",
                           @"rightAnswer": @"Iron",
                           @"used": @"NO"};
        self.question3 = @{@"question": @"What company is NOT assosiated with games?",
                           @"alternative1": @"Electronic Arts",
                           @"alternative2": @"Huawei",
                           @"alternative3": @"Steam",
                           @"alternative4": @"Dice",
                           @"rightAnswer": @"Huawei",
                           @"used": @"NO"};
        self.question4 = @{@"question": @"Which of these is not a bear species?",
                           @"alternative1": @"Water Bear",
                           @"alternative2": @"Polar Bear",
                           @"alternative3": @"Grizzly Bear",
                           @"alternative4": @"Brown Bear",
                           @"rightAnswer": @"Water Bear",
                           @"used": @"NO"};
        self.question5 = @{@"question": @"What is the capital of Lithuania?",
                           @"alternative1": @"Riga",
                           @"alternative2": @"Talin",
                           @"alternative3": @"Kaunas",
                           @"alternative4": @"Vilnius",
                           @"rightAnswer": @"Vilnius",
                           @"used": @"NO"};
        self.question6 = @{@"question": @"How many sides does a tringle have?",
                           @"alternative1": @"Two",
                           @"alternative2": @"Three",
                           @"alternative3": @"Four",
                           @"alternative4": @"None",
                           @"rightAnswer": @"Two",
                           @"used": @"NO"};
        self.question7 = @{@"question": @"Who painted Mona Lisa?",
                           @"alternative1": @"Picasso",
                           @"alternative2": @"Da Vinci",
                           @"alternative3": @"Michelangelo",
                           @"alternative4": @"Van Gogh",
                           @"rightAnswer": @"Da Vinci",
                           @"used": @"NO"};
        self.question8 = @{@"question": @"From what Sci-Fi universe do Cylons come from?",
                           @"alternative1": @"Star Gate",
                           @"alternative2": @"Star Wars",
                           @"alternative3": @"Battlestar Galactica",
                           @"alternative4": @"Star Trek",
                           @"rightAnswer": @"Battlestar Galactica",
                           @"used": @"NO"};
        self.question9 = @{@"question": @"What does IP stand for(Computer Networking term)?",
                           @"alternative1": @"Internal Protocol",
                           @"alternative2": @"Internet Property",
                           @"alternative3": @"Internet Protocol",
                           @"alternative4": @"Intercom Profile",
                           @"rightAnswer": @"Internet Protocol",
                           @"used": @"NO"};
        self.question10 = @{@"question": @"What was Facebook called before it became Facebook?",
                           @"alternative1": @"Thefacebook",
                           @"alternative2": @"Facemash",
                           @"alternative3": @"ConnectU",
                           @"alternative4": @"HarvardConnection",
                           @"rightAnswer": @"Thefacebook",
                           @"used": @"NO"};
        
        self.questions = @[self.question0, self.question1, self.question2, self.question3, self.question4, self.question5, self.question6, self.question7, self.question8, self.question9, self.question10];
        self.usedQuestions=[[NSMutableArray alloc] init];
    }
    
    return self;
}

-(NSDictionary*)randomQuestion {
    NSDictionary *newQuestion = self.questions[arc4random() % self.questions.count];
    if ([self hasThisQuestionBeenUsed:newQuestion]) {
        [self randomQuestion];
    }
    return newQuestion;
}

-(BOOL)answer1:(NSDictionary*)question {
    if ([question[@"alternative1"] isEqualToString:question[@"rightAnswer"]]) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)answer2:(NSDictionary*)question {
    if ([question[@"alternative2"] isEqualToString:question[@"rightAnswer"]]) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)answer3:(NSDictionary*)question {
    if ([question[@"alternative3"] isEqualToString:question[@"rightAnswer"]]) {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)answer4:(NSDictionary*)question {
    if ([question[@"alternative4"] isEqualToString:question[@"rightAnswer"]])  {
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)hasThisQuestionBeenUsed:(NSDictionary*)questionToCheck {
    
    if ([questionToCheck[@"used"] isEqualToString:@"NO"]) {
            [self.usedQuestions setValue:@"YES" forKey:@"used"];
            return NO;
        }
    return YES;
}


@end

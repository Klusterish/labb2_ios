//
//  ViewController.m
//  Labb_iOS_2.0
//
//  Created by Kluster on 2016-02-06.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import "ViewController.h"
#import "QuizBackend.h"

@interface ViewController ()
@property (strong, nonatomic) IBOutlet QuizBackend *quizBackend;
@property (weak, nonatomic) IBOutlet UITextView *questionField;
@property (weak, nonatomic) IBOutlet UIButton *alternative1;
@property (weak, nonatomic) IBOutlet UIButton *alternative2;
@property (weak, nonatomic) IBOutlet UIButton *alternative3;
@property (weak, nonatomic) IBOutlet UIButton *alternative4;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestion;
@property (weak, nonatomic) IBOutlet NSDictionary *customQuestion;
@property (weak, nonatomic) IBOutlet UITextField *resultField;

@end

@implementation ViewController

- (QuizBackend*)quizBackend {
    if(!_quizBackend) {
        _quizBackend = [[QuizBackend alloc] init];
    }
    return _quizBackend;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.customQuestion = self.quizBackend.randomQuestion;
    [self startRound];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)startRound {
    
    self.questionField.text = self.customQuestion[@"question"];
    [self.alternative1 setTitle:self.customQuestion[@"alternative1"] forState:UIControlStateNormal];
    [self.alternative2 setTitle:self.customQuestion[@"alternative2"] forState:UIControlStateNormal];
    [self.alternative3 setTitle:self.customQuestion[@"alternative3"] forState:UIControlStateNormal];
    [self.alternative4 setTitle:self.customQuestion[@"alternative4"] forState:UIControlStateNormal];
    
}
- (IBAction)nextQuestion:(UIButton *)sender {
    self.customQuestion = self.quizBackend.randomQuestion;
    [self startRound];
    NSLog(@"%@", self.customQuestion);
    self.resultField.text = @"";
}
- (IBAction)guess1:(UIButton *)sender {
    if ([self.quizBackend answer1:self.customQuestion]) {
        self.resultField.text = @"Correct";
    } else {
        self.resultField.text = @"Wrong";
    }
}
- (IBAction)guess2:(UIButton *)sender {
    if ([self.quizBackend answer2:self.customQuestion]) {
        self.resultField.text = @"Correct";
    } else {
        self.resultField.text = @"Wrong";
    }
}
- (IBAction)guess3:(UIButton *)sender {
    if ([self.quizBackend answer3:self.customQuestion]) {
        self.resultField.text = @"Correct";
    } else {
        self.resultField.text = @"Wrong";
    }
}
- (IBAction)guess4:(UIButton *)sender {
    if ([self.quizBackend answer4:self.customQuestion]) {
        self.resultField.text = @"Correct";
    } else {
        self.resultField.text = @"Wrong";
    }
}

@end

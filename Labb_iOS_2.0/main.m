//
//  main.m
//  Labb_iOS_2.0
//
//  Created by Kluster on 2016-02-06.
//  Copyright © 2016 Oleg Lindvin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
